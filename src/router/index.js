import Vue from 'vue'
import firebase from 'firebase'
import Router from 'vue-router'
import HomeClient from '@/components/Client/HomeClient.vue'
import Products from '@/components/Client/Products.vue'
import SignIn from '@/components/Client/SignIn.vue'
import SignUp from '@/components/Client/SignUp.vue'
import HomeAdmin from '@/components/CMS/HomeAdmin.vue'
import ProductAdd from '@/components/CMS/ProductAdd.vue'
import ProductEdit from '@/components/CMS/ProductEdit.vue'
import ProductsList from '@/components/CMS/ProductsList.vue'
import ProductDetails from '@/components/Client/ProductDetails.vue'
import Users from '@/components/CMS/Users.vue'
import UserDetails from '@/components/CMS/UserDetails.vue'

Vue.use(Router)
let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HomeClient',
      component: HomeClient
    },
    {
      path: '/products',
      name: 'Products',
      component: Products
    },
    {
      path: '/signIn',
      name: 'SignIn',
      component: SignIn
    },
    {
      path: '/signUp',
      name: 'SignUp',
      component: SignUp
    },
    {
      path: '/admin',
      name: 'HomeAdmin',
      component: HomeAdmin,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/addProduct',
      name: 'ProductAdd',
      component: ProductAdd,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/productDetails/:id',
      name: 'ProductDetails',
      component: ProductDetails
    },
    {
      path: '/productsList',
      name: 'ProductsList',
      component: ProductsList,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/editProduct/:id',
      name: 'ProductEdit',
      component: ProductEdit,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/users',
      name: 'Users',
      component: Users,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/userDetails/:id',
      name: 'UserDetails',
      component: UserDetails,
      meta: {
        requiresAuth: true
      }
    }
  ]
})

// permet de rediriger vers le login si pas authentifier
router.beforeEach((to, from, next) => {
  let currentUser = firebase.auth().currentUser
  let requiresAuth = to.matched.some(record => record.meta.requiresAuth)

  if (requiresAuth && !currentUser) next('signIn')
  else next()
})

export default router
