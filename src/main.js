// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import VueFirestore from 'vue-firestore'
import Firebase from 'firebase'
require('firebase/firestore')

Vue.use(VueFirestore)
Vue.use(BootstrapVue)

Vue.config.productionTip = false

var firebaseApp = Firebase.initializeApp({
  apiKey: 'AIzaSyCsVcrPsZj1RlvfWL1JKqtJbc7FFjXX3kE',
  authDomain: 'vue-firestore-b0d1d.firebaseapp.com',
  databaseURL: 'https://vue-firestore-b0d1d.firebaseio.com',
  projectId: 'vue-firestore-b0d1d',
  storageBucket: 'vue-firestore-b0d1d.appspot.com',
  messagingSenderId: '1009886193289'
})
Firebase.auth().onAuthStateChanged(function (user) {
  new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App/>'
  })
})
export const firestore = firebaseApp.firestore()
export const firebase = firebaseApp
